import pexpect

class TestClass:
    def test_correct_2(self):
        process = pexpect.spawn('./myapp/usr/bin/myapp')
        process.expect('Введите количество элементов в массиве:'.encode('utf-8'))
        process.sendline('2'.encode('utf-8'))
        process.expect('Введите число:'.encode('utf-8'))
        process.sendline('1'.encode('utf-8'))
        process.expect('Введите число:'.encode('utf-8'))
        process.sendline('2'.encode('utf-8'))
        process.expect('Элементы массива: 1, 2'.encode('utf-8'))
        process.expect('Наибольший элемент: 2'.encode('utf-8'))
        process.close()

    def test_correct_3(self):
        process = pexpect.spawn('./myapp/usr/bin/myapp')
        process.expect('Введите количество элементов в массиве:'.encode('utf-8'))
        process.sendline('3'.encode('utf-8'))
        process.expect('Введите число:'.encode('utf-8'))
        process.sendline('1'.encode('utf-8'))
        process.expect('Введите число:'.encode('utf-8'))
        process.sendline('3'.encode('utf-8'))
        process.expect('Введите число:'.encode('utf-8'))
        process.sendline('2'.encode('utf-8'))
        process.expect('Элементы массива: 1, 3, 2'.encode('utf-8'))
        process.expect('Наибольший элемент: 3'.encode('utf-8'))
        process.close()

