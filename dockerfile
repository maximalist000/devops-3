FROM ubuntu:latest
ADD . /app
WORKDIR /app
RUN apt update
RUN apt install make
RUN apt install g++ -y
RUN make init
RUN make build
RUN apt install pip -y 
RUN pip install -r requirements.txt
RUN python3 -m pytest tests.py -k TestClass